#Declaring function to evaluate each grid
def walk(grid):
	switch = ["R", "D", "L", "U"]
	M = int(grid[0])
	N = int(grid[1])
	count = -1
	while (M > 0 and N > 0):
		M -= 1
		count += 1
		if (N > 0 and M > 0):
			N -= 1
			count += 1
	return switch[count%4]

#Start
lines = []
directions = []
while True:
	line = input()
	if line:
		lines.append(line.split())
	else:
		break
T = int(lines.pop(0)[0])
if (T > len(lines)):
	raise ValueError("The first value can't be greater than the number of grids.")

#Traversing user input
i = 0
while (i < T):
	directions.append(walk(lines[i]))
	i += 1

#Printing result
print('\n'.join(directions))