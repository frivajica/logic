#Define roman equivalences
roman_eq = {
	0: "",
	1: "I",
	2: "II",
	3: "III",
	4: "IV",
	5: "V",
	6: "VI",
	7: "VII",
	8: "VIII",
	9: "IX",
	10: "X",
	20: "XX",
	30: "XXX",
	40: "XL",
	50: "L",
	60: "LX",
	70: "LXX",
	80: "LXXX",
	90: "XC",
	100: "C",
	200: "CC",
	300: "CCC",
	400: "CD",
	500: "D",
	600: "DC",
	700: "DCC",
	800: "DCCC",
	900: "CM",
	1000: "M",
	2000: "MM",
	3000: "MMM"
}

#Define functions
def extract_years(interval_string):
	years = []
	year_strings = interval_string.split("-")
	for year_string in year_strings:
		year = (int(year_string[0:-2])) #extract the year from the string
		if (year_string[-2:] == 'BC'):
			years.append( (year * -1) + 754)
		else:
			years.append( (year) + 753)
	return years

def to_roman_notation(num):
	digits = str(num)
	output = []
	for i, digit in enumerate(digits):
		expanded = int(digit) * (10 ** (len(digits)-i-1)) #turn digits into expanded notation
		output.append( roman_eq[expanded] ) #replace notation with roman equivalence
	return "".join(output)

def min_viable(interval):
	max = 0
	[current, last] = extract_years(interval)
	while (current <= last):
		current_length = len(to_roman_notation(current))
		if (current_length > max):
			max = current_length
		current += 1
	return max

#Start
print(min_viable("753BC-747BC"))	#Should be 3
print(min_viable("2000AD-2012AD"))	#Should be 10
print(min_viable("1BC-1AD"))		#Should be 7